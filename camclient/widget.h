﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>
#include <QTimer>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_connectPushButton_clicked();
    void on_connected();
    void on_disconnected();
    void updatepic();

private:
    Ui::Widget *ui;
    QTcpSocket  *socket;
    QTimer  *timer;
    QPixmap *pixmap;
};

#endif // WIDGET_H
