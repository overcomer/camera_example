#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <error.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "tcp.h"
#include "jpg.h"

extern pthread_mutex_t jpg_mutex;

ssize_t readn(int fd, void *vptr, size_t n)
{
	size_t  nleft;
	ssize_t nread;
	char   *ptr;
	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ( (nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR)
				nread = 0;      /* and call read() again */
			else
				return (-1);
		} else if (nread == 0)
			break;              /* EOF */
		nleft -= nread;
		ptr += nread;
	}
	return (n - nleft);         /* return >= 0 */
}

ssize_t writen(int fd, const void *vptr, size_t n)
{
	size_t nleft;
	ssize_t nwritten;
	const char *ptr;
	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR)
				nwritten = 0;   /* and call write() again */
			else
				return (-1);    /* error */
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return (n);
}

int client_process(int connfd, struct jpg_buf_t *jpg)
{
	unsigned int piclen = 0;
	unsigned char * buf ;
	char c;

	while(1)
	{
		readn(connfd, &c, 1);
		pthread_mutex_lock(&jpg_mutex);
		piclen = jpg->len;
		printf("piclen: %d\n", piclen);
		buf = (unsigned char *)malloc(piclen);
		memcpy(buf, jpg->buf, piclen);
		pthread_mutex_unlock(&jpg_mutex);

		if (writen(connfd, &piclen, sizeof piclen) < 0) {
			free(buf);
			break;
		}
		if (writen(connfd, buf, piclen) < 0) {
			free(buf);
			break;
		}
		free(buf);
	}
	close(connfd);
	return 0;
}

